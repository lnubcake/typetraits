﻿#include "pch.h"
#include <iostream>

int main()
{
    std::cout << "Hello World!\n"; 
}

// IsDerived<Base, Derived>::value: leszármazottja-e (ez laborfeladat is).
// IsSame<A, B>::value: ugyanaz a típus - e.
// IsFunction<T>::value : függvény - e, pl. int(bool) függvény, mert int f(bool);
// IsArray<T>::value: igaz, ha T tömb.
// RemoveExtent<T>::type : levesz a tömbből egy dimenziót.T[x]→T, T[x][y]→T[x].
// RemoveAllExtents<T>::type : az összes dimenziót leveszi : T[x][y]...→T, amúgy általában T→T.
// ArrayDimensions<T>::value : a tömb dimenzióinak száma, pl.T[x][y][z] esetén 3.
// IsIterable<T>::value : megeszi - e az std::begin() függvény.
// HasRandomAccessIterator<T>::value : mint az előző, de az is feltétel, hogy random access iterálható legyen.
// IsCopiable<T>::value : van - e másoló konstruktora(a konstruktora átvesz - e balértéket, pl.az std::cout nem másolható).
// IsMovable<T>::value : van - e mozgató konstruktora(átvesz - e jobbértéket). (Lehet, hogy ilyenkor is másolás történik, de nem azt kell ellenőrizni.)
// IsPolymorphic<T>::value : van - e virtuális függvénye.
// IsEmptyClass<T>::value : igaz, ha üres a T osztály, nincs adattagja, pl. struct X {}.
// IsEnum<T>::value : igaz, ha egy enum, amúgy hamis.
// IsAbstract<T>::value : igaz, ha T absztrakt osztály, amúgy hamis.
// Decay<T>::type : a benne lévő típus a T típus függvényparaméter átadáskori transzformáltja, pl. int const → int, int[10] → int*, int& → int.
// IsSigned<T>::value : igaz, ha a T típus előjeles egész(bármekkora), hamis, ha nem előjeles. (Csak egész típusokra kell működjön.)
